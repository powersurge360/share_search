from crispy_forms.helper import FormHelper
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        super(LoginForm, self).__init__(*args, **kwargs)


class RegistrationForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        try:
            if (not self.cleaned_data["password"]
                    == self.cleaned_data["confirm_password"]):

                raise forms.ValidationError(
                    "Password must match confirm password"
                )
        except KeyError:
            pass
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        super(RegistrationForm, self).__init__(*args, **kwargs)
