from storages.backends.s3boto import S3BotoStorage


S3MediaStorage = lambda: S3BotoStorage(location='media')
S3StaticStorage = lambda: S3BotoStorage(location='static')
