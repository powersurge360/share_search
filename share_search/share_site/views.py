from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.generic import FormView, RedirectView

from share_site.forms import LoginForm, RegistrationForm


class LoginView(FormView):
    template_name = "share_site/login.html"
    form_class = LoginForm

    def get_success_url(self):
        next_url = self.request.REQUEST.get("next")
        if next_url:
            return next_url

        return reverse("home")

    def form_valid(self, form):
        user = authenticate(
            username=self.request.POST.get("username"),
            password=self.request.POST.get("password"),
        )

        if user:
            login(self.request, user)

        return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView):
    def get_redirect_url(*args, **kwargs):
        return reverse("home")

    def get(self, *args, **kwargs):
        logout(self.request)
        return super(LogoutView, self).get(*args, **kwargs)


class RegistrationView(FormView):
    form_class = RegistrationForm
    template_name = "share_site/register.html"

    def form_valid(self, form):
        User.objects.create_user(
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password"],
            email=form.cleaned_data["email"],
        )

        user = authenticate(
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password"],
        )

        login(self.request, user)

        return super(RegistrationView, self).form_valid(form)

    def get_success_url(*args, **kwargs):
        return reverse("home")
