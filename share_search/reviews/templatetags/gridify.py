from django import template

register = template.Library()


@register.assignment_tag
def gridify(list_, columns):
    index = 0
    for i in list_:
        new_list = list_[index:index + columns]
        if new_list:
            yield new_list
        index += columns
