from django import forms

from reviews.models import Store, PhotoModel, Product


class StoreForm(forms.ModelForm):
    class Meta:
        model = Store
        fields = ("name", "description", "image")


class PhotoForm(forms.ModelForm):
    class Meta:
        model = PhotoModel
        fields = ("description", "image", "store")

        widgets = {
            "store": forms.HiddenInput()
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ("name", "description", "image")
