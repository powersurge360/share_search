from django.db import models


class ReviewModel(models.Model):
    """Abstract class for core review values"""
    body = models.TextField()
    body_markup = models.TextField()
    rating = models.IntegerField(
        choices=(zip(range(1, 6), range(1, 6)))
    )
    votes = models.IntegerField()

    class Meta:
        abstract = True


class ProfileModel(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    description_html = models.TextField()

    class Meta:
        abstract = True


class Store(ProfileModel):
    image = models.ImageField(upload_to="store_profile")

    @models.permalink
    def get_absolute_url(self):
        return ("reviews:store", (self.pk,))


class PhotoModel(models.Model):
    image = models.ImageField(upload_to="profile_photos")
    description = models.TextField()
    store = models.ForeignKey(Store, related_name="photos")


class Product(ProfileModel):
    image = models.ImageField(upload_to="product_profile")

    @models.permalink
    def get_absolute_url(self):
        return ("reviews:product", (self.pk,))


class ProductReview(ReviewModel):
    store = models.ForeignKey(Store, related_name="product_reviews")
    product = models.ForeignKey(Product, related_name="reviews")


class StoreReview(ReviewModel):
    store = models.ForeignKey(Store, related_name="reviews")
