# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Store'
        db.create_table(u'reviews_store', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('description_html', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'reviews', ['Store'])

        # Adding model 'Product'
        db.create_table(u'reviews_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('description_html', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'reviews', ['Product'])

        # Adding model 'StorePhoto'
        db.create_table(u'reviews_storephoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('votes', self.gf('django.db.models.fields.IntegerField')()),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photos', to=orm['reviews.Store'])),
        ))
        db.send_create_signal(u'reviews', ['StorePhoto'])

        # Adding model 'ProductPhoto'
        db.create_table(u'reviews_productphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('votes', self.gf('django.db.models.fields.IntegerField')()),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photos', to=orm['reviews.Product'])),
        ))
        db.send_create_signal(u'reviews', ['ProductPhoto'])

        # Adding model 'ProductReview'
        db.create_table(u'reviews_productreview', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('body_markup', self.gf('django.db.models.fields.TextField')()),
            ('rating', self.gf('django.db.models.fields.IntegerField')()),
            ('votes', self.gf('django.db.models.fields.IntegerField')()),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(related_name='product_reviews', to=orm['reviews.Store'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reviews', to=orm['reviews.Product'])),
        ))
        db.send_create_signal(u'reviews', ['ProductReview'])

        # Adding model 'StoreReview'
        db.create_table(u'reviews_storereview', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('body_markup', self.gf('django.db.models.fields.TextField')()),
            ('rating', self.gf('django.db.models.fields.IntegerField')()),
            ('votes', self.gf('django.db.models.fields.IntegerField')()),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reviews', to=orm['reviews.Store'])),
        ))
        db.send_create_signal(u'reviews', ['StoreReview'])


    def backwards(self, orm):
        # Deleting model 'Store'
        db.delete_table(u'reviews_store')

        # Deleting model 'Product'
        db.delete_table(u'reviews_product')

        # Deleting model 'StorePhoto'
        db.delete_table(u'reviews_storephoto')

        # Deleting model 'ProductPhoto'
        db.delete_table(u'reviews_productphoto')

        # Deleting model 'ProductReview'
        db.delete_table(u'reviews_productreview')

        # Deleting model 'StoreReview'
        db.delete_table(u'reviews_storereview')


    models = {
        u'reviews.product': {
            'Meta': {'object_name': 'Product'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_html': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'reviews.productphoto': {
            'Meta': {'object_name': 'ProductPhoto'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['reviews.Product']"}),
            'votes': ('django.db.models.fields.IntegerField', [], {})
        },
        u'reviews.productreview': {
            'Meta': {'object_name': 'ProductReview'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'body_markup': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reviews'", 'to': u"orm['reviews.Product']"}),
            'rating': ('django.db.models.fields.IntegerField', [], {}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product_reviews'", 'to': u"orm['reviews.Store']"}),
            'votes': ('django.db.models.fields.IntegerField', [], {})
        },
        u'reviews.store': {
            'Meta': {'object_name': 'Store'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_html': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'reviews.storephoto': {
            'Meta': {'object_name': 'StorePhoto'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['reviews.Store']"}),
            'votes': ('django.db.models.fields.IntegerField', [], {})
        },
        u'reviews.storereview': {
            'Meta': {'object_name': 'StoreReview'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'body_markup': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rating': ('django.db.models.fields.IntegerField', [], {}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reviews'", 'to': u"orm['reviews.Store']"}),
            'votes': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['reviews']