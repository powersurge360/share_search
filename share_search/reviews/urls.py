from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from reviews.views import (StoreDetailView, StoreListView,
                           StoreUpdateView, ProductListView,
                           ProductDetailView, StoreCreateView,
                           PhotoModelCreateView,
                           ProductCreateView, ProductUpdateView)


urlpatterns = patterns(
    '',
    url(r'^stores/$', StoreListView.as_view(), name="store_list"),
    url(r'^stores/(?P<pk>\d+)/$', StoreDetailView.as_view(), name="store"),
    url(
        r'^stores/edit/(?P<pk>\d+)/$',
        StoreUpdateView.as_view(),
        name="store_edit"
    ),
    url(
        r'^products/edit/(?P<pk>\d+)/$',
        ProductUpdateView.as_view(),
        name="product_edit"
    ),
    url(r'^stores/create/$', StoreCreateView.as_view(), name="store"),
    url(r'^products/$', ProductListView.as_view(template_name="reviews/products.html"), name="product_list"),
    url(r'^products/$', TemplateView.as_view(template_name="reviews/products.html"), name="product_list"),
    url(r'^stores/create/$', StoreCreateView.as_view(), name="store_create"),
    url(r'^products/create/$', ProductCreateView.as_view(), name="product_create"),
    url(
        r'^products/(?P<pk>\d+)/$',
        ProductDetailView.as_view(),
        name="product",
    ),
    url(
        r'^photos/create/$',
        PhotoModelCreateView.as_view(),
        name="photo_create"
    ),
)
