from braces.views import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, ListView, CreateView, UpdateView

from reviews.forms import StoreForm, ProductForm, PhotoForm
from reviews.models import Store, Product, PhotoModel


class StoreDetailView(DetailView):
    model = Store

    def get_context_data(self, *args, **kwargs):
        context = super(StoreDetailView, self).get_context_data(
            *args,
            **kwargs
        )

        context["photo_form"] = PhotoForm()
        return context


class StoreListView(ListView):
    model = Store


class StoreCreateView(LoginRequiredMixin, CreateView):
    model = Store
    form_class = StoreForm


class StoreUpdateView(LoginRequiredMixin, UpdateView):
    model = Store
    form_class = StoreForm


class ProductDetailView(DetailView):
    model = Product


class ProductListView(ListView):
    model = Product


class PhotoModelCreateView(LoginRequiredMixin, CreateView):
    model = PhotoModel
    form_class = PhotoForm

    def get_success_url(self, *args, **kwargs):
        if self.object:
            return reverse("reviews:store", args=(self.object.store.pk,))
        else:
            return reverse(
                "reviews:store",
                args=(self.request.POST.get("store"),)
            )

    def form_invalid(self, form):
        return HttpResponseRedirect(self.get_success_url())


class ProductCreateView(LoginRequiredMixin, CreateView):
    model = Product
    form_class = ProductForm


class ProductUpdateView(LoginRequiredMixin, UpdateView):
    model = Product
    form_class = StoreForm
