$(function() {
    "use strict";

    $(".modal .submit-button").on("click", function() {
        var $this = $(this);

        $this.closest(".modal").find("form").submit();
    });
});
