from .default import *

DEBUG = False
TEMPLATE_DEBUG = False

INSTALLED_APPS = INSTALLED_APPS + (
    'storages',
)

AWS_STORAGE_BUCKET_NAME = "share_search"
AWS_SECRET_ACCESS_KEY = os.environ["AWS_SECRET"]
AWS_ACCESS_KEY_ID = os.environ["AWS_ACCESS_KEY"]
DEFAULT_FILE_STORAGE = 'share_site.storage.S3MediaStorage'
STATICFILES_STORAGE = 'share_site.storage.S3StaticStorage'
STATIC_URL = "http://%s.s3.amazonaws.com/static/" % AWS_STORAGE_BUCKET_NAME
MEDIA_URL = "http://%s.s3.amazonaws.com/media/" % AWS_STORAGE_BUCKET_NAME

ALLOWED_HOSTS = ["share-search.herokuapp.com", "sharesearch.co"]
